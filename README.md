# Binary Clock Code

This repository includes various bits of code for binary and hex clocks.
It started out in Python but spread to other languages as convenient.
The ideas behind it are described at http://x31eq.com/clock.html


## Python Clocks

``binarytime.py`` is a Python module for showing the current time in a
binary or hex format.  Run `python binarytime.py --help` for options.
Should be compatible with at least python 2.7 and 3.6 upwards.

Some highlights:

6 bit clock with 4 hour range in Unicode:
``python binarytime.py -6``

Unicode with 4 hour range and 1 second precision:
``python binarytime.py -4xs``

Hex clock with 4 hour range: ``python binarytime.py -4x --digital``

``binaryclock.py`` takes the same options as ``binarytime.py`` but
prints out new times as at the time changes.  It also has options for
setting the title of a shell terminal and placing the time on every
window's title bar using ``wmctrl``.


## C and X Window System Clocks

Type ``make`` to build clocks from C code.  Marvel at unresolved
dependencies like the ``gcc`` compiler and ``libx11-dev``.

``xminuto`` is an X application to show an 8 bit clock with a 4 hour
range and 1 minute precision.  It has options for convenient
foreground or background operation and you can choose the size and
position.  You can also set these options retrospectively with your
window manager or ``wmctrl``.  It uses Xlib, and should work with any
window manger, but I'm not sure about big endian systems.

``xdomino`` is a variation of ``xminuto`` for a 6 bit clock.  It is
built from the same source code (``xminuto.c``).

The intention of these C programs is to be small and simple, and so
require minimal resources when they run.  The "on" bits are black and
the "off" bits are white.  The clock has a 1 pixel white border, and
some small black lines are used to make the edges clear when there's a
lot of white.  I use them on my desktop and they are attractive,
readable and reliable.


## Hexadecimal Time and Date

I also added a couple of shell scripts for showing a time stamp in a
hexadecimal format.  These are examples of simple scripting and don't
include UTF-8 tricks.  Like the binary clocks, they're mostly useless,
but might come in handy for IPv6 addresses or other cases where you want
a simple time stamp in hex with more readability than a simple count of
seconds since the epoc.

``hexstamp`` shows the current date and time with 1 second precision in
ten hex digits.  The format is:

1. Four digits counting quarter-years since the calendar epoch.  This
   corresponds to 14 bits for the year and 4 bits for the quarter.
   (This is not fixed at four digits and can be shorter for historical
   dates.)
2. Two digits counting the month (7 bits counting January as 0) and an
   AM/PM bit.
3. A colon to separate time from date.
4. One digit for the hour of the 12 hour clock counting noon and
   midnight as 0 (really base 12).
5. Three digits with the time in minutes and seconds of the "skip time"
   I use in binary clocks.

``feestamp`` has a few differences:

1. The first two digits count quarter-years over a range of 64 years.
2. The next digit goes up every week and every month, in a way
   that's best explained in the source code.
3. The last digit of the date part counts half-days of the week
   with Sunday morning as 0.
4. The last two digits of the time part count seconds in base 15.
   (Hence the name: the last second of a minute is "fee" (barring leap
   seconds.))

The scripts require a shell such as ``ksh``, ``bash`` or ``zsh``
but otherwise should be POSIX-compatible for showing the current time and
date, but haven't been tested outside Linux.  For showing arbitrary
dates, they require GNU date and wrap GNU date's time format.

There's also a script called ``feedate`` that takes the output of
``feestamp`` and turns it into a conventional datetime string.
Note that this script isn't a clock, doesn't involve binary, and isn't
written in Python.  So it represents huge feature creep.  Usage:

1. Argument matches the output of ``feestamp``.
2. Environment variable ``HEXEPOCH`` is the first year of the 64
   that dates will be decoded to.  The default is 1984 for the epoch ending
   with 2047.  Anything other than a multiple of 64 will break.
   This is a good way of testing the tests.

There are some other shell scripts to work with these:

``test`` checks that ``hexstamp`` and ``feestamp`` are correct for some
known times and dates, and work with whatever of ``ksh``, ``bash``, and
``zsh`` are installed.

``longtest`` is a ``zsh``-specific test script that ensures round-trip
consistency for every time of day and every date of the epoch.

``tick`` prints a counter updating every second on the second.  It can
be used with the time stamp scripts to give similar usage to
``binaryclock.py``:

    ./tick | while read; do ./feestamp; done

``updateline`` keeps changing the output line instead of printing a new
one.  For example:

    ./tick | while read; do ./feestamp; done | ./updateline

    python binaryclock.py -6 | ./updateline


## Licensing Details

This code and the ideas behind it are so simple that claiming any
intellectual property protection for it would, as far as I understand
these things, be completely unenforceable.  So I haven't attached a
license, but feel free to do what you like with them and use this
statement to say that I told you so.

In a strange parallel universe, somebody might market a clock using
ideas from this project.  They might attempt to claim intellectual
property protection for such ideas in a destructive manner, claiming all
the profits for sensible binary clocks for their expanding binary clock
empire.  Should this unlikely situation occur in your universe, you can
use this repository as evidence of prior art.

Finally, the disclaimer of liabilities: if use of the code or ideas in
this project causes any harm to you, your business, or innocent third
parties, it's your own stupid fault.
