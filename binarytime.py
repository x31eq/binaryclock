import argparse, sys, re
from datetime import datetime

# fix for python2/3 compatibility
try:
    unicode
    unichr
except NameError:
    unicode = str
    unichr = chr


hex_digits = ''.join(map('{:x}'.format, range(16)))

# Glyphs for squashed output.
# Indexed by base 4 digit.
boxes = ''.join(map(unichr, (0x2581, 0x2584, 0x2580, 0x2588)))

bitboxes = boxes[0] + boxes[-1]

# Glyphs for square output.
# Indexed by the value of the hex digit.
# Values are unicode code points.
squareboxes = ''.join(map(unichr, (
    0x2581, 0x2597, 0x259d, 0x2590,
    0x2596, 0x2584, 0x259e, 0x259f,
    0x2598, 0x259a, 0x2580, 0x259c,
    0x258c, 0x2599, 0x259b, 0x2588,
    )))


def skiptime():
    """
    hours, minutes, seconds, quarter-seconds as binary-fraction numbers.
    """
    now = datetime.now()
    quarter = (now.microsecond*0x4)//1000000
    def skip(i):
        return min(63, i + i // 15)
    return now.hour, skip(now.minute), skip(now.second), quarter


def proportional_time():
    """
    Fractions of an hour grouped like minutes, seconds, and quarters
    """
    now = datetime.now()
    # 6 bits for "minutes", 6 for "seconds" and 2 for quarters
    # = 14 bits for fractions of hours = 0x4000
    second = now.minute * 60 + now.second
    microsecond = second * 1000000 + now.microsecond
    fraction = (microsecond * 0x4000) // (60 * 60 * 1000000)
    pseudominute = fraction >> 8
    pseudosecond = (fraction >> 2) & 0x3f
    quarter = fraction & 0x3
    return now.hour, pseudominute, pseudosecond, quarter


def binary_format(h, m, s, q, hour_range=24):
    """
    Return minutes and quarter-seconds as binary strings
    Format as simple binary
    """
    hr = h % hour_range
    bits = len('{:b}'.format(hour_range - 1))
    hour = '{:06b}'.format(hr)[-bits:]
    qsec = '{:06b}'.format(s) + '{:02b}'.format(q)
    return hour + '{:06b}'.format(m), qsec


def adjust_grouping(m, qs, show_seconds, fast=False, slow=False):
    """
    Regroup binary strings for minutes, and quarter-seconds
    into something-like-minutes and something-like-seconds
    according to the options
    """
    if slow:
        qs = m[-2:] + qs[:-2]
        m = m[:-2]
    if show_seconds:
        return m, qs if fast else qs[:6]
    return m + qs[:2] if fast else m, ''


def adjust_fives(skipvalue):
    quarter, remainder = divmod(skipvalue, 16)
    short = 3 if remainder == 14 else remainder // 5
    ticks = short * 4
    return ticks + min(remainder - ticks, 3) + quarter * 16


def minsec(hour_range, show_seconds=True,
           fast=True, slow=True, fives=False,
           proportional=False):
    """
    Return something like minutes, seconds
    """
    if proportional:
        h, m, s, q = proportional_time()
    else:
        h, m, s, q = skiptime()
    if fives:
        slow = True
        if show_seconds:
            s = adjust_fives(s)
        else:
            m = adjust_fives(m)
    m, qs = binary_format(h, m, s, q, hour_range)
    return adjust_grouping(m, qs, show_seconds, fast, slow)


def rebase(mins, secs, nbits,
           digits=hex_digits, delimiter=':',
           reflect=False):
    """
    Format the minutes and seconds (binary strings)
    in groups of n bits
    and encode them as "digits".
    Optional reflection.
    """
    assert 0 < nbits < 5

    # Break the minutes back into something like hours and minutes
    minute_bits = (0, 6, 6, 6, 8)[nbits]
    max_minute_bits = (0, 6, 8, 6, 8)[nbits]

    if len(mins) - max_minute_bits <= 1:
        # Only make a separate watch if
        # the number of bits for hours-and-minutes
        # is more than one bit beyond the largest allowed.
        watches = ''
        if len(mins) - 1 in (minute_bits, max_minute_bits):
            # Move a bit to seconds to give a good minute size
            secs = mins[-1:] + secs
            mins = mins[:-1]
    else:
        # Assign any overflow to be "watches"
        watches, mins = mins[:-minute_bits], mins[-minute_bits:]
        # left pad watches
        watches = '0000'[:(-len(watches) % nbits)] + watches

    # Borrow seconds or zeros to right-pad minutes
    underflow = -len(mins) % nbits
    mins += (secs + '0000')[:underflow]
    secs = secs[underflow:]

    # Trim seconds if there's only a 1-bit overflow
    if (len(secs) % nbits) == 1:
        secs = secs[:-1]
    else:
        # Right pad with zeros
        secs += '0000'[:(-len(secs) % nbits)]

    assert len(watches) % nbits == 0
    assert len(mins) % nbits == 0
    assert len(secs) % nbits == 0

    tokens = list(filter(None, (watches, mins, secs)))

    assert len(digits) >= (1 << nbits)

    if reflect and nbits == 4 and digits == squareboxes:
        tokens += [''.join(re.findall('[01][01]', token)[::-1])
                   for token in tokens][::-1]
        reflect = False

    tokens = [''.join(digits[int(group, 2)]
                      for group in re.findall('[01]' * nbits, t))
              for t in tokens]

    if reflect:
        tokens += [t[::-1] for t in tokens][::-1]

    return delimiter.join(tokens)


def time_text(args):
    """
    Return the current time given the command-line arguments
    """
    hour_range = (4 if args.sextadiem or args.domino else
                  8 if args.tridiem else
                  12 if args.halfday else
                  24)
    slow = args.slow or args.domino
    mins, secs = minsec(hour_range,
                        show_seconds=args.seconds,
                        fast=args.fast,
                        slow=slow,
                        fives=args.fives,
                        proportional=args.proportional,
                        )
    nbits = (4 if args.hex or args.domino else
             2 if args.squash else
             1)
    if 0 < len(args.chars) < (1 << nbits):
        sys.stderr.write('Not enough characters\n')
        sys.exit(3)
    digits = (args.chars if args.chars else
              hex_digits if args.digital else
              (bitboxes, boxes, bitboxes, squareboxes)[nbits - 1])
    return rebase(mins, secs, nbits, digits, args.delim, args.reflect)


parser = argparse.ArgumentParser(
        description="Command line binary clock")
parser.add_argument('-s', '--seconds',
        action='store_true',
        help="Show the seconds.")
parser.add_argument('-12', '--12-hour',
        action='store_true',
        dest='halfday',
        help="12 hour clock")
parser.add_argument('-8', '--tridiem',
        action='store_true',
        help="8 hour scope")
parser.add_argument('-4', '--sextadiem',
        action="store_true",
        help="4 hour scope")
parser.add_argument ('-6', '--domino',
        action="store_true",
        help="6 bit clock (equivalent to -4x --slow)"),
parser.add_argument('-5', '--fives',
        action="store_true",
        help="Tick over on 5-minute or 5-second boundaries")
parser.add_argument('-p', '--proportional',
        action="store_true",
        help="Divide into 64 minute and second-like divisions")
parser.add_argument('--slow', '-w',
        action="store_true",
        help="Lose the last one or two bits")
parser.add_argument('--fast', '-f',
        action="store_true",
        help="Add one or two bits")
parser.add_argument('--hex', '-x',
        action="store_true",
        help="Squashed with 4 bits to a character")
parser.add_argument('--squash', '-q',
        action="store_true",
        help="2 bits to a character (base 4)")
parser.add_argument('--digital',
        action="store_true",
        help="Forget binary display and show normal digits")
parser.add_argument('--chars',
        type=unicode,
        default='',
        help="Provide a string to use to encode the digits")
parser.add_argument('--delim', '-d',
        type=unicode,
        default=':',
        help="Delimiter between pieces of time")
parser.add_argument('--reflect',
        action="store_true",
        help="Show time with horizontal reflection")


if __name__ == '__main__':
    text = time_text(parser.parse_args())
    if not sys.stdout.encoding:
        text = text.encode('utf-8')
    print(text)
