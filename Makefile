all: xdomino xminuto

xdomino: xminuto.c
	gcc xminuto.c -O2 -DDOMINO -o xdomino -lX11 --std=gnu11 -Wall

xminuto: xminuto.c
	gcc xminuto.c -O2 -o xminuto -lX11 --std=gnu11 -Wall
