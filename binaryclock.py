#!/usr/bin/env python

import time, sys
import binarytime
import subprocess

# fix for python2/3 compatibility
if hasattr(binarytime, 'unicode'):
    from binarytime import unicode

def write(text):
    if not sys.stdout.encoding:
        text = text.encode('utf-8')
    return sys.stdout.write(text)

binarytime.parser.add_argument('--title',
        type=unicode,
        default=None,
        help="Set the terminal title. "
             "The argument is a template with {time} "
             "to be substituted")
binarytime.parser.add_argument('--titles',
        action='store_true',
        help="Change the titles of all windows")
args = binarytime.parser.parse_args()

if args.titles:
    if not args.squash:
        args.hex = True

if args.proportional:
    # 36e2 / 0x1000 == 15./16*15/16 == 1/0.87890625
    # 60 * 15./16 == 0.87890625 * 64 == 56.25
    sleep_time = 0.87890625 if args.seconds else 56.25
else:
    sleep_time = 1.0 if args.seconds else 60
if args.fast and not args.slow:
    sleep_time /= 4


def update_clock(args):
    text = binarytime.time_text(args)
    if args.title:
        write("\x1b]2;")
        write(args.title.format(time=text))
        write("\x07")
    elif args.titles:
        try:
            windows = subprocess.check_output(['wmctrl', '-l'])
        except subprocess.CalledProcessError as cpe:
            sys.stderr.write('Unable to get window list: {}\n'
                                .format(cpe))
            return
        for line in windows.splitlines():
            try:
                window, desktop, host, title = line.split(None, 3)
            except ValueError:
                sys.stderr.write('Indecipherable line from wmctrl: {}\n'
                                 .format(repr(line)))
            oldtitle = title.decode('utf-8')
            if oldtitle.startswith(text):
                continue
            while oldtitle and oldtitle[0] in binarytime.squareboxes:
                oldtitle = oldtitle[1:]
            if not oldtitle.startswith('ImageMagick'):
                try:
                    subprocess.check_call(['wmctrl', '-ir', window, '-N',
                                            text + oldtitle])
                except subprocess.CalledProcessError as cpe:
                    sys.stderr.write('Unable to update window: {}\n'
                                        .format(cpe))
    else:
        if '\n' in text:
            write('\n')
        write(text)
        write('\n')
    sys.stdout.flush()
    time.sleep(-time.time() % sleep_time)


if __name__=='__main__':
    try:
        while True:
            update_clock(args)
    except KeyboardInterrupt:
        # Quietly exit with the usual code
        sys.exit(130)
    except IOError as ioe:
        if ioe.errno == 32:
            # Broken pipe, exit quietly
            sys.exit(0)
        raise
